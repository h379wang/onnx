FROM ubuntu:16.04
MAINTAINER Heli Wang "h379wang@edu.uwaterloo.ca"

RUN apt-get update
RUN apt-get install -y default-jre
RUN apt-get install -y default-jdk
RUN apt-get install dialog apt-utils -y
RUN apt-get -y install curl
RUN apt-get -y install wget

# Enable Sudo Mode
RUN apt-get install sudo
RUN adduser --disabled-password --gecos '' docker
RUN adduser docker sudo
RUN echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

# Install Dependencies
RUN apt-get update && apt-get install -y --no-install-recommends apt-utils
RUN apt-get install -y build-essential git
RUN apt-get install -y libatlas-base-dev
RUN apt-get install -y libopenblas-dev liblapack-dev
RUN apt-get install -y libopencv-dev
RUN update-alternatives --config java
RUN apt-get install -y maven
RUN apt-get install -y scala

USER docker

# Install package ccache
RUN sudo apt install -y ccache --assume-yes
RUN sudo /usr/sbin/update-ccache-symlinks
RUN echo 'export PATH="/usr/lib/ccache:$PATH"' | tee -a ~/.bashrc
RUN export PATH="/usr/lib/ccache:$PATH"

WORKDIR /app/
RUN sudo wget -O l_mkl_2018.2.199.tgz http://registrationcenter-download.intel.com/akdlm/irc_nas/tec/12725/l_mkl_2018.2.199.tgz
RUN sudo tar -xzf l_mkl_2018.2.199.tgz
WORKDIR /app/l_mkl_2018.2.199
RUN sudo apt-get install -y cpio
COPY ./l_mkl_2018.2.199/silent.cfg /app/l_mkl_2018.2.199/silent.cfg
RUN sudo ./install.sh -s silent.cfg

COPY ./incubator-mxnet /app/incubator-mxnet
WORKDIR /app/incubator-mxnet

#RUN source /opt/intel/bin/compilervars.sh intel64
Run make -j ${nproc}

RUN sudo make scalapkg
# Install Mxnet Scala - Build from source
RUN sudo make scalainstall

# Install Python2/3 & PyTorch
#RUN sudo add-apt-repository ppa:deadsnakes/ppa
RUN sudo apt-get update
RUN sudo apt-get install -y python-dev python-setuptools python-pip libgfortran3 python3.5 python3-pip --assume-yes
RUN sudo pip3 install numpy==1.13.3

# Install Mxnet (Python)
WORKDIR python
RUN sudo pip3 install -e .
RUN sudo apt-get install graphviz --assume-yes
RUN sudo pip3 install graphviz==0.8.1
WORKDIR /

COPY ./installprotobuf.sh /app/installprotobuf.sh
RUN sudo bash /app/installprotobuf.sh

COPY ./onnx /app/onnx
WORKDIR /app/
RUN sudo pip3 install --upgrade pybind11
RUN python3 -m pybind11 --includes
RUN sudo pip3 install -e onnx/
RUN sudo pip3 uninstall torch -y # uninstall the built-in version of torch
RUN sudo pip3 install torch==0.4.0
RUN sudo pip3 install https://github.com/pytorch/text/archive/master.zip

#EXPOSE 5000
#ENTRYPOINT ["python"]
#CMD ["main.py"]
